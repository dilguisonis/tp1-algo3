import time
import os
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import random
import sys
import numpy as np

  
def armar_instancia_fuerza_bruta(cant_elementos):
    file1 = open("instancias/fuerza_bruta_input", "w")  # write mode

    file1.write(str(cant_elementos) + "\n")
    file1.write(str(cant_elementos) + "\n")
    for i in range(1, cant_elementos):
        file1.write(str(10) + " " + str(10) + "\n")
        #file1.write(str(random.randint(0, 10)) + " " + str(random.randint(0, 10)) + "\n") 
    file1.close()
  
def armar_instancia_dinamica(cant_elementos, resistencia_jambotubo):
    file1 = open("instancias/dinamica_input", "w")  # write mode

    file1.write(str(cant_elementos) + "\n")
    file1.write(str(resistencia_jambotubo) + "\n")
    peso = 1
    for i in range(1, cant_elementos):
        file1.write(str(peso) + " " + str(peso) + "\n")
    file1.close()

def armar_instancia_backtracking_factibilidad_mejor_caso(cant_elementos):
    nombre_archivo = "instancias/backtracking_factibilidad_mejor_caso_input"
    file1 = open(nombre_archivo, "w") 

    file1.write(str(cant_elementos) + "\n") # n
    file1.write(str(1000) + "\n") # R
    for i in range(cant_elementos):
        file1.write(str(2) + " " + str(1) + "\n")
    file1.close()

    return nombre_archivo

def armar_instancia_backtracking_factibilidad_peor_caso(cant_elementos):
    file1 = open("instancias/backtracking_factibilidad_peor_caso_input", "w") 

    file1.write(str(cant_elementos) + "\n")
    file1.write(str(cant_elementos) + "\n")
    for i in range(1, cant_elementos):
        file1.write(str(1) + " " + str(cant_elementos) + "\n")
    file1.close()    
    
def armar_instancia_backtracking_optimalidad_mejor_caso(cant_elementos):
    nombre_archivo = "instancias/backtracking_optimalidad_mejor_caso_input"
    file1 = open(nombre_archivo, "w") 

    file1.write(str(cant_elementos) + "\n") # n
    file1.write(str(1000) + "\n") # R
    for i in range(cant_elementos):
        file1.write(str(1) + " " + str(1000) + "\n")
    file1.close()

    return nombre_archivo

def armar_instancia_backtracking_optimalidad_peor_caso(cant_elementos):
    file1 = open("instancias/backtracking_optimalidad_peor_caso_input", "w")

    file1.write(str(cant_elementos) + "\n")
    file1.write(str(0) + "\n")
    for i in range(1, cant_elementos):
        file1.write(str(cant_elementos) + " " + str(1) + "\n")
    file1.close()


#-----------------------------------------------------------------------------------------------------------------------------------------------#
    
def medir_tiempos(algoritmo, input):
    if algoritmo != 'FB' and algoritmo != 'BT' and algoritmo != 'BT-O' and algoritmo != 'BT-F' and algoritmo != 'DP':
        print("algoritmo {} no reconocido".format(algoritmo))
        sys.exit()
    tiempo_inicial = time.time()

    process = os.system('./tp {} < {}'.format(algoritmo, input))

    tiempo_final = time.time()
    
    return tiempo_final - tiempo_inicial


def medir_tiempos_fuerza_bruta():	
    tiempo_inicial = time.time()

    process = os.system('./tp FB < instancias/fuerza_bruta_input')

    tiempo_final = time.time()
    
    return tiempo_final - tiempo_inicial

def medir_tiempos_backtracking():	
    tiempo_inicial = time.time()

    process = os.system('./tp BT < instancias/backtracking_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial
    
def medir_tiempos_backtracking_factibilidad_mejor_caso():	
    tiempo_inicial = time.time()

    process = os.system('./tp BT-F < instancias/backtracking_factibilidad_mejor_caso_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial

def medir_tiempos_backtracking_factibilidad_peor_caso():	
    tiempo_inicial = time.time()

    process = os.system('./tp BT-F < instancias/backtracking_factibilidad_peor_caso_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial
    
def medir_tiempos_backtracking_optimalidad_mejor_caso():	
    tiempo_inicial = time.time()

    process = os.system('./tp BT-O < instancias/backtracking_optimalidad_mejor_caso_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial

def medir_tiempos_backtracking_optimalidad_peor_caso():	
    tiempo_inicial = time.time()

    process = os.system('./tp BT-O < instancias/backtracking_optimalidad_peor_caso_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial

def medir_tiempos_programacion_dinamica():	
    tiempo_inicial = time.time()

    process = os.system('./tp PD < instancias/programacion_dinamica_input')

    tiempo_final = time.time()

    return tiempo_final - tiempo_inicial
    
    
#-----------------------------------------------------------------------------------------------------------------------------------------------#
#Prueba con funciones de complejidad


def complejidad_fb():
    experimentofile = open("complejidad_fb.csv", "w")
    #experimentofile.write("cant_elementos,tiempo\n")
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")
    for cant_elementos in range(1, 31, 1):
        nombre_archivo_mejor_caso_bt_f = armar_instancia_backtracking_factibilidad_mejor_caso(cant_elementos)
        resultado_experimento_mejor_caso_bt_f = medir_tiempos('FB', nombre_archivo_mejor_caso_bt_f)

        nombre_archivo_mejor_caso_bt_o = armar_instancia_backtracking_optimalidad_mejor_caso(cant_elementos)
        resultado_experimento_mejor_caso_bt_o = medir_tiempos('FB', nombre_archivo_mejor_caso_bt_o)

        experimentofile.write(str(cant_elementos) + "," + str((2**cant_elementos)*0.000000003) + "," + str("O(2^n)*0.000000003") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento_mejor_caso_bt_o) + "," + str("Fuerza bruta para mejor-caso-BT-O") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento_mejor_caso_bt_f) + "," + str("Fuerza bruta para mejor-caso-BT-F") + "\n")

def plotear_complejidad_fb():
    df = pd.read_csv('complejidad_fb.csv')
    plot = sns.lineplot(data=df, x='cant_elementos', y='tiempo', hue='algoritmo')
    plt.subplots(figsize=(3, 2))
    plt.show()


def armar_instancia_backtracking_peor_caso(cant_elementos):
    nombre_archivo = "instancias/backtracking_peor_caso_input"
    file1 = open(nombre_archivo, "w") 

    file1.write(str(cant_elementos) + "\n") # n
    file1.write(str(50) + "\n") # R
    for i in range(cant_elementos):
        file1.write(str(1) + " " + str(8) + "\n")
    file1.close()

    return nombre_archivo

def complejidad_bt():
    experimentofile = open("complejidad_bt.csv", "w")
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")
    for cant_elementos in range(1, 31, 1):
        nombre_archivo_peor_caso_bt = armar_instancia_backtracking_peor_caso(cant_elementos)
        resultado_experimento_peor_caso_bt = medir_tiempos('BT', nombre_archivo_peor_caso_bt)

        experimentofile.write(str(cant_elementos) + "," + str((2**cant_elementos)*0.0000000002) + "," + str("O(2^n)*0.0000000002") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento_peor_caso_bt) + "," + str("BT para peor-caso-BT") + "\n")

def plotear_complejidad_bt():
    df = pd.read_csv('complejidad_bt.csv')
    plot = sns.lineplot(data=df, x='cant_elementos', y='tiempo', hue='algoritmo')
    plt.subplots(figsize=(3, 2))
    plt.show()

def plotear_complejidad_bt_pearson():
    df = pd.read_csv('complejidad_bt.csv')
    df["complejidad"] = 2 ** df["cant_elementos"]
    fig = sns.lmplot(data=df, x="tiempo", y="complejidad")
    print("Pearson entre tiempos BT y su complejidad: {}".format(correlacion_pearson(df, 'tiempo', 'complejidad')))
    plt.subplots(figsize=(3, 2))
    plt.show()


def plotear_complejidad_fb_pearson():
    df = pd.read_csv('complejidad_fb.csv')
    df = df[df['algoritmo'] == "Fuerza bruta para mejor-caso-BT-O"]
    df["complejidad"] = 2 ** df["cant_elementos"]
    fig = sns.lmplot(data=df, x="tiempo", y="complejidad")
    print("Pearson entre tiempos PD y su complejidad: {}".format(correlacion_pearson(df, 'tiempo', 'complejidad')))
    plt.subplots(figsize=(3, 2))
    plt.show()

def experimento_1():
    experimentofile = open("experimento1.csv", "w")
    #experimentofile.write("cant_elementos,tiempo\n")
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")
    FB = "fuerza bruta"
    BT = "backtracking"
    PD = "programacion dinamica"
    for cant_elementos in range(1, 30, 1):
        #armar_instancia_grande(cant_elementos)
        #resultado_experimento = medir_tiempos1()
        #experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento*0.00000035) + "\n")
        #experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento) + "," + str(funcion3) + "\n")

        armar_instancia_backtracking_optimalidad_peor_caso(cant_elementos)
        resultado_experimento = medir_tiempos_backtracking_optimalidad_peor_caso()
        experimentofile.write(str(cant_elementos) + "," + str(2^cant_elementos*0.00000035) + "," + str("O(2^n)") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_experimento) + "," + str(BT) + "\n")
        
        #experimentofile.write(str(cant_elementos) + "," + str((2**cant_elementos)*(cant_elementos**2)*0.000000048) + "\n")
        #experimentofile.write(str(cant_elementos) + "," + str((2**cant_elementos)*(cant_elementos**2)*0.000000048*(10**(-300))) + "," + str(funcion4) + "\n")
        
        #experimentofile.write(str(cant_elementos) + "," + str((2**cant_elementos)*0.00000035) + "\n")
        #experimentofile.write(str(cant_elementos) + "," + str(cant_elementos*100*0.00000035) + "\n")
        

def plotear_experimento_1():
    data_exp1 = pd.read_csv("experimento1.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()
    
#-----------------------------------------------------------------------------------------------------------------------------------------------#   
       
def generar_instancia_comparacion_podas_mejor_caso_BT_F(): 
    resultado_FB = []
    resultado_BT_O = []
    resultado_BT_F = []
    resultado_BT = []
    for cant_elementos in range(1, 30):
        nombre_archivo_instancia = armar_instancia_backtracking_factibilidad_mejor_caso(cant_elementos)
        print("Corriendo BT-O con {} elementos".format(cant_elementos))
        resultado_BT_O.append(medir_tiempos('BT-O', nombre_archivo_instancia))
        print("Corriendo FB con {} elementos".format(cant_elementos))
        resultado_FB.append(medir_tiempos('FB', nombre_archivo_instancia))
        print("Corriendo BT-F con {} elementos".format(cant_elementos))
        resultado_BT_F.append(medir_tiempos('BT-F', nombre_archivo_instancia))
        print("Corriendo BT con {} elementos".format(cant_elementos))
        resultado_BT.append(medir_tiempos('BT', nombre_archivo_instancia))

    # Creamos y escribimos el resultado del tiempo empleado para calcular FB y BT-F para cada n desde 1 hasta 10.000, saltando de a 10 en un archivo.
    archivo_resultados = "comparacion_podas_mejor_caso_BT_F.csv"
    experimentofile = open(archivo_resultados, "w")

    # Esta linea es para poder plotear en la función de ploteo.
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")

    # ejecutamos cada paso para cada n:
    # cant_elementos es "n"
    index = 0
    for cant_elementos in range(1, 30, 1):
        # Escribimos en el archivo de resultados la linea: n, tiempo de ejecución, algoritmo
        experimentofile.write(str(cant_elementos) + "," + str(resultado_FB[index]) + "," + str("Fuerza Bruta") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT_F[index]) + "," + str("BT con poda de factibilidad") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT_O[index]) + "," + str("BT con poda de optimalidad") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT[index]) + "," + str("BT con ambas podas") + "\n")
        index += 1

    return archivo_resultados

       
def generar_instancia_comparacion_podas_mejor_caso_BT_O(): 
    resultado_FB = []
    resultado_BT_O = []
    resultado_BT_F = []
    resultado_BT = []
    for cant_elementos in range(1, 30):
        nombre_archivo_instancia = armar_instancia_backtracking_optimalidad_mejor_caso(cant_elementos)
        print("Corriendo BT-O con {} elementos".format(cant_elementos))
        resultado_BT_O.append(medir_tiempos('BT-O', nombre_archivo_instancia))
        print("Corriendo FB con {} elementos".format(cant_elementos))
        resultado_FB.append(medir_tiempos('FB', nombre_archivo_instancia))
        print("Corriendo BT-F con {} elementos".format(cant_elementos))
        resultado_BT_F.append(medir_tiempos('BT-F', nombre_archivo_instancia))
        print("Corriendo BT con {} elementos".format(cant_elementos))
        resultado_BT.append(medir_tiempos('BT', nombre_archivo_instancia))

    archivo_resultados = "comparacion_podas_mejor_caso_BT_O.csv"
    experimentofile = open(archivo_resultados, "w")

    # Esta linea es para poder plotear en la función de ploteo.
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")

    # ejecutamos cada paso para cada n:
    # cant_elementos es "n"
    index = 0
    for cant_elementos in range(1, 30, 1):
        # Escribimos en el archivo de resultados la linea: n, tiempo de ejecución, algoritmo
        experimentofile.write(str(cant_elementos) + "," + str(resultado_FB[index]) + "," + str("Fuerza Bruta") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT_F[index]) + "," + str("BT con poda de factibilidad") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT_O[index]) + "," + str("BT con poda de optimalidad") + "\n")
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT[index]) + "," + str("BT con ambas podas") + "\n")
        index += 1

    return archivo_resultados


#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

def generar_instancia_FB_vs_BT_F_peor_caso(): 
    resultado_FB = []
    for cant_elementos in range(1, 30, 1):
        print("Corriendo FB con {} elementos".format(cant_elementos))
        # Creamos el archivo de input
        armar_instancia_fuerza_bruta(cant_elementos)
        # Calculamos el tiempo que se tarda en ejecutar FB con el input creado
        resultado_FB.append(medir_tiempos_fuerza_bruta())

    resultado_BT_F = []
    for cant_elementos in range(1, 30, 1):
        print("Corriendo BT-F con {} elementos".format(cant_elementos))
        # Creamos el archivo de input
        armar_instancia_backtracking_factibilidad_peor_caso(cant_elementos)
        # Calculamos el tiempo que se tarda en ejecutar BT-F con el input creado
        resultado_BT_F.append(medir_tiempos_backtracking_factibilidad_peor_caso())
    
    # Creamos y escribimos el resultado del tiempo empleado para calcular FB y BT-F para cada n desde 1 hasta 10.000, saltando de a 10 en un archivo.
    experimentofile = open("FB_vs_BT_F_peor_caso.csv", "w")

    # Esta linea es para poder plotear en la función de ploteo.
    experimentofile.write("cant_elementos,tiempo,algoritmo\n")

    # ejecutamos cada paso para cada n:
    # cant_elementos es "n"
    index = 0
    for cant_elementos in range(1, 30, 1):
        # Escribimos en el archivo de resultados la linea: n, tiempo de ejecución, "Fuerza Bruta"
        experimentofile.write(str(cant_elementos) + "," + str(resultado_FB[index]) + "," + str("Fuerza Bruta") + "\n")
        # Escribimos en el archivo de resultados la linea: n, tiempo de ejecución, "BT con poda de factibilidad"
        experimentofile.write(str(cant_elementos) + "," + str(resultado_BT_F[index]) + "," + str("BT con poda de factibilidad") + "\n")
        index += 1


#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

def experimento_dinamica():
    nombre_archivo_resultados = "resultados_dinamica.csv"
    experimentofile = open(nombre_archivo_resultados, "w")
    experimentofile.write("cant_elementos,resistencia_jambotubo,tiempo,algoritmo\n")
    resultado_DP = []
    for cant_elementos in range(100, 4000, 100):
        for resistencia in range(100, 4000, 100):
            print("Corriendo DP con {} elementos y resistencia de jambotubo {}".format(cant_elementos, resistencia))
            armar_instancia_dinamica(cant_elementos, resistencia)
            resultado_DP = medir_tiempos('DP', "instancias/dinamica_input")
            experimentofile.write(str(cant_elementos) + "," + str(resistencia) + "," + str(resultado_DP) + "," + str("DP") + "\n")
    return nombre_archivo_resultados

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

def plotear_backtracking_peor_caso_vs_mejor_caso():
    data_exp1 = pd.read_csv("backtracking_peor_caso_vs_mejor_caso.csv")

    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()
    
def plotear_backtracking_factibilidad():
    data_exp1 = pd.read_csv("backtracking_factibilidad.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()

def plotear_backtracking_optimalidad():
    data_exp1 = pd.read_csv("backtracking_optimalidad.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()
    
def plotear_backtracking_factibilidad_vs_optimalidad():
    data_exp1 = pd.read_csv("backtracking_factibilidad_vs_optimalidad.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()
    
def plotear_FB_vs_BT_F_mejor_caso():
    data_exp1 = pd.read_csv("FB_vs_BT_F_mejor_caso.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()

def plotear_FB_vs_BT_F_peor_caso():
    data_exp1 = pd.read_csv("FB_vs_BT_F_peor_caso.csv")

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.show()

def plotear_lineplot(archivo_resultados):
    data_exp1 = pd.read_csv(archivo_resultados)

    #sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo')
    plot = sns.lineplot(data=data_exp1, x='cant_elementos', y='tiempo', hue='algoritmo')
    #plot.set(ylim=(0, 0.04))
    plt.subplots(figsize=(3, 2))
    plt.show()
 
def plotear_variando_R_PD(archivo_resultados):
    df_dinamica = pd.read_csv(archivo_resultados)
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==500], x='resistencia_jambotubo', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==1000], x='resistencia_jambotubo', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==1500], x='resistencia_jambotubo', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==2000], x='resistencia_jambotubo', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==2500], x='resistencia_jambotubo', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["cant_elementos"]==3000], x='resistencia_jambotubo', y='tiempo')
    fig.legend(labels=['n=500','n=1000','n=1500','n=2000','n=2500','n=3000'])
    fig.set(xlabel='R', ylabel='tiempo (ms)')
    plt.subplots(figsize=(3, 2))
    plt.show()
 
def plotear_variando_n_PD(archivo_resultados):
    df_dinamica = pd.read_csv(archivo_resultados)
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==500], x='cant_elementos', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==1000], x='cant_elementos', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==1500], x='cant_elementos', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==2000], x='cant_elementos', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==2500], x='cant_elementos', y='tiempo')
    fig = sns.scatterplot(data=df_dinamica[df_dinamica["resistencia_jambotubo"]==3000], x='cant_elementos', y='tiempo')
    fig.legend(labels=['R=500','R=1000','R=1500','R=2000','R=2500','R=3000'])
    fig.set(xlabel='n', ylabel='tiempo (ms)')
    plt.subplots(figsize=(3, 2))
    plt.show()

def plotear_heatmap_pd(archivo_resultados):
    df_dinamica = pd.read_csv(archivo_resultados)
    df_heatmap = df_dinamica.pivot_table(index='cant_elementos', columns='resistencia_jambotubo', values='tiempo', aggfunc=np.mean)
    fig = sns.heatmap(df_heatmap)
    fig.invert_yaxis()
    plt.subplots(figsize=(3, 2))
    plt.show()

def correlacion_pearson(data_frame, columna1, columna2):
    return np.corrcoef(data_frame[columna1], data_frame[columna2])[0,1]

def plotear_pearson_PD(archivo_resultados):
    # Graficamos los tiempos de ejecución de la serie de instancias X1, ..., Xn vs la complejidad esperada.
    df_dinamica = pd.read_csv(archivo_resultados)
    df_dinamica["complejidad"] = df_dinamica["cant_elementos"] * df_dinamica["resistencia_jambotubo"]
    fig = sns.scatterplot(data=df_dinamica, x="tiempo", y="complejidad")
    print("Pearson entre tiempos PD y su complejidad: {}".format(correlacion_pearson(df_dinamica, 'tiempo', 'complejidad')))
    plt.show()
    #print("Índice de correlación de Pearson:", correlacion_pearson(df_dinamica, "tiempo", "complejidad"))

#generar_instancia_FB_vs_factibilidad_CON_FB()
#generar_instancia_FB_vs_factibilidad_CON_FACTIBILIDAD()


#generar_instancia_FB_vs_BT_F_peor_caso()
#plotear_FB_vs_BT_F_peor_caso()

#generar_instancia_FB_vs_BT_F_mejor_caso()
#plotear_FB_vs_BT_F_mejor_caso()
   
#generar_instancia_backtracking_factibilidad_vs_optimalidad()
#plotear_backtracking_factibilidad_vs_optimalidad()
    
#generar_instancia_backtracking_peor_caso_vs_mejor_caso()
#plotear_backtracking_peor_caso_vs_mejor_caso()

#generar_instancia_backtracking_optimalidad()
#plotear_backtracking_optimalidad()

#generar_instancia_backtracking_factibilidad()
#plotear_backtracking_factibilidad()

# ================================

# Exp comparacion de podas de BT
#archivo_resultados_podas_mejor_caso_BT_F = generar_instancia_comparacion_podas_mejor_caso_BT_F()
#plotear_lineplot(archivo_resultados_podas_mejor_caso_BT_F)

#archivo_resultados_podas_mejor_caso_BT_O = generar_instancia_comparacion_podas_mejor_caso_BT_O()
#plotear_lineplot(archivo_resultados_podas_mejor_caso_BT_O)

# exp complejidad dinamica
#archivo_resultados_dinamica = experimento_dinamica()
#archivo_resultados_dinamica ='resultados_dinamica.csv'
#plotear_heatmap_pd('resultados_dinamica.csv')
#plotear_variando_n_PD(archivo_resultados_dinamica)
#plotear_variando_R_PD(archivo_resultados_dinamica)
#plotear_pearson_PD(archivo_resultados_dinamica)

#experimento_1()
#plotear_experimento_1()

#complejidad_fb()
#plotear_complejidad_fb()
#plotear_complejidad_fb_pearson()

complejidad_bt()
#plotear_complejidad_bt()
plotear_complejidad_bt_pearson()