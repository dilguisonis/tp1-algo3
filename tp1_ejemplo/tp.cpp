#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <set>
#include <map>

using namespace std;

int INFTY = 10e6; // Valor para indicar que no hubo solución.

// Información de la instancia a resolver.

vector<int> S; //Vector que nos dio el tp de ejemplo
vector<int> r_aux; //Vector de resistencias
vector<int> w_aux; //Vector de pesos

int n;
int W;
int mejor_solucion = 0;
//i: Indice.
//R: Resistencia actual.


/*

int FB(int i, int R) // LISTO
{
    // Caso base.
    if (i < 0) return 0;

    // Recursión.
    if (R < 0) //Si la resistencia es menor a 0, esta solución no me sirve.
    {
        ///return 0;
        return -INFTY + FB(i - 1, R);  //"invalidamos" solución
    }
    else{
        return max(1 +  FB(i - 1, min(R - w_aux[i], r_aux[i])),
                   FB(i - 1, R));
    }
}

*/

/*

def fn(i, R, j):

  if (R < 0 and i >= 5): return -1000
  if (R < 0): return -1000 + fn(i + 1, R, j)
  if (i >= 5): return 0
  return max(1 +  fn(i + 1, min(R - w_aux[i], r_aux[i]),j+1), fn(i + 1, R, j))
*/


int FB(int i, int R) // LISTO
{
	// Caso base.
  if (R < 0 && i >= n) return -INFTY;
  if (i >= n) return 0;
    	// Recursión.
  if (R < 0) return -INFTY + max(1 +  FB(i + 1, min(R - w_aux[i], r_aux[i])), FB(i + 1, R));
 
  return max(1 +  FB(i + 1, min(R - w_aux[i], r_aux[i])), FB(i + 1, R));
}

/*

int FB(int i, int R) // LISTO
{
    // Caso base.
    if (i == n) return 0;

    // Recursión.
    if (R < 0) //Si la resistencia es menor a 0, esta solución no me sirve.
    {
        ///return 0;
        return -INFTY + FB(i + 1, R);  //"invalidamos" solución
    }    
    else{
        return max(1 +  FB(i + 1, min(R - w_aux[i], r_aux[i])),
                   FB(i + 1, R));
    }
}

*/

// i: posicion del elemento a considerar en este nodo.
// k: cantidad de elementos seleccionados hasta este nodo.
// w: suma de los elementos seleccionados hasta este nodo.

/*
int getMin(vector<int> v, int n)
{
    int res = v[n];
    for (int i = n; i >= 0; i--)
        res = min(res, v[i]);
    return res;
}
*/

int getMin(vector<int> v, int i)
{
    int res = v[n];
    for (int j = i; j < n; j++)
        res = min(res, v[j]);
    return res;
}

bool poda_factibilidad = true; // define si la poda por factibilidad esta habilitada.
bool poda_optimalidad = true; // define si la poda por optimalidad esta habilitada.
int K = INFTY; // Mejor solucion hasta el momento.

/*
int BT(int i, int R)
{
    // Caso base.
    if (i < 0)
    {return 0;}
    if(poda_factibilidad) {
        if (R < 0) {
            return -INFTY;        // poda por factibilidad
        }
    }
    if(poda_optimalidad) {
        if (R < getMin(w_aux, i)) {
            return 0;    // poda por optimalidad
        }
    }
    // Recursión.
    return max(1 +  FB(i - 1, min(R - w_aux[i], r_aux[i])),
               FB(i - 1, R));
}
*/

int BT(int i, int R, int s)
{
    // Caso base.
	if (R < 0 && i >= n) { return -INFTY; }
	
	if(poda_factibilidad) {
		if (R < 0) { return -INFTY; }        // poda por factibilidad
		
	}
	
	// Actualizo la mejor solucion en caso de haber encontrado una mayor.
	
	if (R >= 0 && s > mejor_solucion) { mejor_solucion = s; }
        
	if (i >= n) { return 0; }
        
    /*
 //   if(poda_optimalidad) {
        if (R < getMin(w_aux, i)) { return 0; }    // poda por optimalidad
    */
    
	if(poda_optimalidad) {
    	
    		if (mejor_solucion >= s + n - i) {
                return 0; }
    		
    	}
    	
    	// si la mejor solucion hasta ahora es mayor que la solucion actual + la cantidad de elementos totales restantes que se puedan meter al jambotubo,
    	// entonces es imposible alcanzar la mejor solucion y devuelvo 0.
    
    // Recursión.
    	return max(1 +  BT(i + 1, min(R - w_aux[i], r_aux[i]), s + 1), BT(i + 1, R, s));

}


/*

int BT(int i, int R)
{
    // Caso base.
    if (i >= n)
    {return 0;}
 //   if(poda_factibilidad) {
        if (R < 0) {
            return -INFTY;        // poda por factibilidad
        }
    
 //   if(poda_optimalidad) {
        if (R < getMin(w_aux, i)) {
            return 0;    // poda por optimalidad
        }
    
    // Recursión.
    return max(1 +  BT(i + 1, min(R - w_aux[i], r_aux[i])),
               BT(i + 1, R));
}

*/

vector<vector<int>> M; // Memoria de PD.
const int UNDEFINED = -1;
int PD(int i, int R, vector<vector<int>> & pi)
{
    if (R < 0) {
        return -INFTY;
    }
    if (i >= n) {
        return 0;
    }

    if (pi[i][R] != UNDEFINED){
        return pi[i][R];
    }

    int actual = max(1 + PD(i + 1, min(R - w_aux[i], r_aux[i]), pi),
                     PD(i + 1, R, pi));

    pi[i][R] = actual;
	
    return actual;
}

// Recibe por parámetro qué algoritmos utilizar para la ejecución separados por espacios.
// Imprime por clog la información de ejecución de los algoritmos.
// Imprime por cout el resultado de algun algoritmo ejecutado.
int main(int argc, char** argv)
{
    // Leemos el parametro que indica el algoritmo a ejecutar.
    map<string, string> algoritmos_implementados = {
            {"FB", "Fuerza Bruta"}, {"BT", "Backtracking con podas"}, {"BT-F", "Backtracking con poda por factibilidad"},
            {"BT-O", "Backtracking con poda por optimalidad"}, {"DP", "Programacion dinámica"}
    };

    cerr << "Verificando que el algoritmo exista" << endl;
    // Verificar que el algoritmo pedido exista.
    if (argc < 2 || algoritmos_implementados.find(argv[1]) == algoritmos_implementados.end())
    {
        cerr << "Algoritmo no encontrado: " << argv[1] << endl;
        cerr << "Los algoritmos existentes son: " << endl;
        for (auto& alg_desc: algoritmos_implementados) cerr << "\t- " << alg_desc.first << ": " << alg_desc.second << endl;
        return 0;
    } else {
        cerr << "Algoritmo existente: " << argv[1] << endl;
    }
    string algoritmo = argv[1];

    cerr << "Esperando n y W" << endl;
    // Leemos el input.
    cin >> n >> W;
    cerr << "Haciendo assigns" << endl;
    w_aux.assign(n, 0);
    r_aux.assign(n, 0);
    //n = r_aux.size();

    cerr << "Esperando inputs" << endl;
    for (int i = 0; i < n; ++i) {
        cin >> w_aux[i] >> r_aux[i];
    }
    cerr << "Verificando cual algoritmo hay que ejecutar" << endl;
    // Ejecutamos el algoritmo y obtenemos su tiempo de ejecución.
    int optimum;
    optimum = INFTY;
    auto start = chrono::steady_clock::now();

    if (algoritmo == "FB")
    {
        optimum = FB(0, W);
    }    
    else if (algoritmo == "BT")
    {
        K = INFTY;
        poda_optimalidad = poda_factibilidad = true;
        optimum = BT(0, W, 0);
    }
    else if (algoritmo == "BT-F")
    {
        K = INFTY;
        poda_optimalidad = false;
        poda_factibilidad = true;
        optimum = BT(0, W, 0);
    }
    else if (algoritmo == "BT-O")
    {
        mejor_solucion = 0;
        std::cout << "corriendo BT-O!" << std::endl;
        K = INFTY;
        poda_optimalidad = true;
        poda_factibilidad = false;
        optimum = BT(0, W, 0);
    }
    else if (algoritmo == "DP")
    {
        // Precomputamos la solucion para los estados.
        M = vector<vector<int>>(n, vector<int>(W+1, UNDEFINED));
/*
        for (int j = 0; j < W+1; ++j)
            for (int i = 0; i < n; ++i)
                PD(i, j, M);
*/
        // Obtenemos la solucion optima.
        optimum = PD(0, W, M);
    } else {
        cerr << "Error" << endl;
        return 0;
    }

    /*5 50
     * 0:   10 45
     * 1:   20 8
     * 2:   30 15
     * 3:   10 2
     * 4:   15 30
     */
    auto end = chrono::steady_clock::now();
    double total_time = chrono::duration<double, milli>(end - start).count();

    // Imprimimos el tiempo de ejecución por stderr.
    clog << total_time << endl;

    // Imprimimos el resultado por stdout.
    cout << (optimum == INFTY ? -1 : optimum) << endl;
    return 0;
}
